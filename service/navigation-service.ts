import { NavigationActions, NavigationParams, StackActions, } from "react-navigation";



export class NavigationService {
  private static navigator: any;
  static initNavigationService(navigatorRef: any) {
    NavigationService.navigator = navigatorRef;
  }

  static navigateAndReset(routeName, params?: NavigationParams) {
    NavigationService.navigator.dispatch(
      StackActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({
            routeName,
            params,
          }),
        ],
      })
    );
  }
  static navigate(routerName: any, params?: NavigationParams) {
    NavigationService.navigator.dispatch(
      NavigationActions.navigate({
        routeName: routerName,
        params
      })
    );
  }
  static goBack() {

    NavigationService.navigator.dispatch(NavigationActions.BACK)

  }
  static getCurrentLocation() {
    var _navigator = NavigationService.navigator;
    let route = _navigator.state.nav;
    while (route.routes) {
      route = route.routes[route.index];
    }
    return route;
  }
}
