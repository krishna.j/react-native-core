import { AxiosRequestConfig, AxiosResponse } from "axios";


export class FetchHttpClient {
  private static token: string;
  private static baseUrl: string;
  static createInstance(baseUrl: string) {
    this.baseUrl = baseUrl;
  }
  static async setToken(token) {
    this.token = token;
  }
  static async setContextToken(token) {
    this.token = token;
  }




  static getClient() {
  }
  static post(
    url: string,
    isAuthenticatedRoot: boolean,
    data: any,
    config?: AxiosRequestConfig
  ): Promise<AxiosResponse> {

    return new Promise<AxiosResponse>((resolve: any, reject: any) => {
      config = config || { headers: { "Content-Type": "application/json" } };

      if (config) {
        if (isAuthenticatedRoot)
          config.headers["Authorization"] = "Bearer " + FetchHttpClient.token;
      }
      let _baseUrl = this.baseUrl;
      fetch(`${_baseUrl}${url}`, {
        method: "POST",
        ...config,
        body: JSON.stringify(data)
      })
        .then(response => {
          response
            .json()
            .then(json => {
              resolve({ status: response.status, data: json });
            })
            .catch(() => {

            });
        })
        .catch(error => reject(error));
    });
  }

  static get(
    url: string,
    isAuthenticatedRoot: boolean,
    config?: AxiosRequestConfig
  ): Promise<AxiosResponse> {
    return new Promise<AxiosResponse>((resolve: any, reject: any) => {
      config = config || { headers: { "Content-Type": "application/json" } };

      if (isAuthenticatedRoot) {
        config.headers["Authorization"] = "Bearer " + FetchHttpClient.token;
      }

      fetch(`${this.baseUrl}${url}`, {
        method: "GET",
        ...config
      })
        .then(response => {

          response.json().then(json => {
            resolve({ status: response.status, data: json });
          });
        })
        .catch(error => reject(error));
    });
  }

  //  static put(
  //     url: string,
  //     data: any,

  //     config: AxiosRequestConfig
  //   ): Promise<AxiosResponse> {
  //     return new Promise<AxiosResponse>((resolve: any, reject: any) => {
  //       this.instance
  //         .put(url, data, config)
  //         .then(response => {
  //           resolve(response);
  //         })
  //         .catch(error => {
  //           reject(error);
  //         });
  //     });
  //   }
  //   delete(
  //     url: string,
  //     isAuthenticatedRoot: boolean,
  //     config?: AxiosRequestConfig
  //   ): Promise<AxiosResponse> {
  //     return new Promise<AxiosResponse>((resolve: any, reject: any) => {
  //       config = config || { headers: { "Content-Type": "application/json" } };

  //       if (isAuthenticatedRoot) {
  //         config.headers["Authorization"] = "Bearer " + FetchHttpClient.token;
  //       }

  //       fetch(`${this.baseUrl}${url}`, {
  //         method: "DELETE",
  //         ...config
  //       })
  //         .then(response => {
  //           consoleLogger("get reponse", response);
  //           response.json().then(json => {
  //             resolve({ status: response.status, data: json });
  //           });
  //         })
  //         .catch(error => reject(error));
  //     });
  //   }
}
