import { check, PERMISSIONS, RESULTS, request } from 'react-native-permissions';
import { Platform } from 'react-native';

export enum PermissionTypes {
    "CAMERA" = "CAMERA"
}
export const PermissionMapping = {
    [PermissionTypes.CAMERA]: {
        "android": PERMISSIONS.ANDROID.CAMERA,
        "ios": PERMISSIONS.IOS.CAMERA
    }

}

export class PermissionService {

    private static async requestPermissionNative(permission: any) {
        return await request(permission)
    }
    private static CheckPermission(permission: PermissionTypes): Promise<any> {
        return new Promise(
            (resolve, reject) => {
                if (Platform.OS == "android") {
                    let responseText = ""
                    check(PermissionMapping[permission].android).then(
                        result => {
                            switch (result) {
                                case RESULTS.DENIED:
                                    this.requestPermissionNative(PermissionMapping[permission].android).then(

                                        result => {
                                            responseText = result
                                        }
                                    )

                                    break

                            }
                            resolve("Permission Requested")
                        }
                    )
                }
                else {
                    return check(PermissionMapping[permission].ios)

                }
            }
        )

    }

    static async requestPermission(permission: PermissionTypes) {
        switch (permission) {
            case PermissionTypes.CAMERA:
                return this.CheckPermission(permission)
                break


        }



    }


}