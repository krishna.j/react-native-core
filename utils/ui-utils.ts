import { Dimensions, PixelRatio, Platform } from "react-native";

export const { width: viewportWidth, height: viewportHeight} = Dimensions.get('window');

export const wp = (percentage: number):number => {
    const value = (percentage * viewportWidth) / 100;
    return Math.round(value);
}

export const hp = (percentage: number):number => {
   
    const value = (percentage * viewportHeight) / 100;
    return Math.round(value);
}
const scale = viewportWidth / 320;

export function normalizeFont(size:number) {
    const newSize = size * scale;
    if (Platform.OS === "ios") {
      return Math.round(PixelRatio.roundToNearestPixel(newSize));
    } else {
      return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2;
    }
  }