export enum IconTypes {
  "AntDesign" = "AntDesign",
  "Entypo" = "Entypo",
  "EvilIcons" = "EvilIcons",
  "Feather" = "Feather",
  "FontAwesome" = "FontAwesome",
  "FontAwesome5" = "FontAwesome5",
  "Foundation" = "Foundation",
  "Ionicons" = "Ionicons",
  "MaterialCommunityIcons" = "MaterialCommunityIcons",
  "MaterialIcons" = "MaterialIcons",
  "Octicons" = "Octicons",
  "SimpleLineIcons" = "SimpleLineIcons",
  "Zocial" = "Zocial"
}
